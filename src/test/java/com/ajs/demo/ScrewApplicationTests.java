package com.ajs.demo;

import com.ajs.demo.mock.entity.Model;
import com.ajs.demo.mock.service.ModelService;
import com.ajs.demo.mock.service.impl.ModelServiceImpl;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;

import static org.mockito.ArgumentMatchers.any;

public class ScrewApplicationTests {

    @InjectMocks
    private ModelService modelService= new ModelServiceImpl();

//    @Mock
//    private ModelDao modelDao;

//    @Before
//    public void setUp() {
//        MockitoAnnotations.initMocks(this);
//
//        when(modelDao.getModel(any(Long.class)))
//                .thenReturn(Model.builder().id(1L).name("model from mock").build());
//    }

    @Test
    public void contextLoads() {
        Model model = modelService.getModel(3L);
        System.out.println(model);
        Assert.assertEquals(3l,model.getId().longValue());
    }

}
