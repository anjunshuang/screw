package com.ajs.demo.time.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @program: screw
 * @description: 时间处理：部分格式化
 * @author: Mr.An
 * @create: 2020-09-09 14:32
 **/
@Data
public class GlobalFormatterVo {

    private LocalDateTime createTime;

    private Date updateTime;
}
