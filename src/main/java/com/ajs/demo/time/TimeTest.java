package com.ajs.demo.time;

import com.ajs.demo.time.vo.GlobalFormatterVo;
import com.ajs.demo.time.vo.PartFormatterVo;
import org.junit.Test;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @program: screw
 * @description: 测试时间处理
 * @author: Mr.An
 * @create: 2020-09-09 14:38
 **/
@RestController
public class TimeTest {

    @GetMapping("/testPartFormatter")
    public PartFormatterVo testPartFormatter1() {
        PartFormatterVo partFormatterVo = new PartFormatterVo();
        partFormatterVo.setCreateTime(LocalDateTime.now());
        partFormatterVo.setUpdateTime(new Date(System.currentTimeMillis()));
        return partFormatterVo;
    }

    @GetMapping("/testGlobalFormatter")
    public GlobalFormatterVo testGlobalFormatter1() {
        GlobalFormatterVo globalFormatterVo = new GlobalFormatterVo();
        globalFormatterVo.setCreateTime(LocalDateTime.now());
        globalFormatterVo.setUpdateTime(new Date(System.currentTimeMillis()));
        return globalFormatterVo;
    }

    @Test
    public void testPartFormatter() {
        PartFormatterVo partFormatterVo = new PartFormatterVo();
        partFormatterVo.setCreateTime(LocalDateTime.now());
        partFormatterVo.setUpdateTime(new Date(System.currentTimeMillis()));
        System.out.println(partFormatterVo);
    }
}
