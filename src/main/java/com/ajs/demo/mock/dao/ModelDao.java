package com.ajs.demo.mock.dao;

import com.ajs.demo.mock.entity.Model;
import org.springframework.stereotype.Repository;

/**
 * @program: screw
 * @description: dao
 * @author: Mr.An
 * @create: 2020-09-14 11:05
 **/
@Repository
public class ModelDao {
    public Model getModel(Long id){
        return Model.builder().id(id).name("model from dao ").build();
    }
}
