package com.ajs.demo.mock.service;

import com.ajs.demo.mock.entity.Model;

/**
 * @author AJ
 */
public interface ModelService {

    /**
     * @param id
     * @return
     */
    Model getModel(Long id);
}
