package com.ajs.demo.mock.service.impl;

import com.ajs.demo.mock.dao.ModelDao;
import com.ajs.demo.mock.entity.Model;
import com.ajs.demo.mock.service.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @program: screw
 * @description: 快速启动测试类
 * @author: Mr.An
 * @create: 2020-09-14 11:01
 **/
@Service
public class ModelServiceImpl implements ModelService {
    @Autowired
    ModelDao modelDao;

    @Override
    public Model getModel(Long id) {
        return Model.builder().id(3L).name("haha").build();
//        return modelDao.getModel(id);
    }
}
