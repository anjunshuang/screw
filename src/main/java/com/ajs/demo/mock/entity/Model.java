package com.ajs.demo.mock.entity;

import lombok.Builder;
import lombok.Data;

/**
 * @program: screw
 * @description: model 实体
 * @author: Mr.An
 * @create: 2020-09-14 11:04
 **/
@Data
@Builder
public class Model {
    private Long id;
    private String name;
}
