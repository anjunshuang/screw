package com.ajs.demo.exception;

import com.ajs.demo.exception.dto.RequestDTO;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @program: screw
 * @description: 全局异常测试类
 * @author: Mr.An
 * @create: 2020-09-10 20:16
 **/
@RestController
public class GlobalExceptionTest {

    @PostMapping("/globalExceptionTest")
    public void addUser(@RequestBody @Valid RequestDTO requestDTO){
        //其余业务处理
        System.out.println(requestDTO.toString());
    }
}
