package com.ajs.demo.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @program: screw
 * @description: JsonResult
 * @author: Mr.An
 * @create: 2020-09-10 20:14
 **/
@Data
@AllArgsConstructor
public class JsonResult<T> {
    private int code;
    private String msg;
    private T data;

//    public R() {
//        this.code = 200;
//        this.message = "SUCCESS";
//    }
//
//    public static <T> R<T> success(T data) {
//        R r = new R();
//        r.setData(data);
//        return r;
//    }
//
//    public static R fail() {
//        R r = new R();
//        r.setCode(500);
//        r.setMessage("FAIL");
//        return r;
//    }
//
//    public static R fail(String message) {
//        R r = new R();
//        r.setCode(500);
//        r.setMessage(message);
//        return r;
//    }
}
