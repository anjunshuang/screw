package com.ajs.demo.exception.dto;


import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotEmpty;


/**
 * @program: screw
 * @description: 请求DTO
 * @author: Mr.An
 * @create: 2020-09-10 20:17
 **/
@Data
@ToString
public class RequestDTO {
    @NotEmpty(message = "名字不能为空")
    String name;

    @NotEmpty(message = "密码不能为空")
    String password;
}
