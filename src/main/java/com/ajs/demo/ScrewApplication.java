package com.ajs.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author AJ
 */
@SpringBootApplication
public class ScrewApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScrewApplication.class, args);
    }

}
