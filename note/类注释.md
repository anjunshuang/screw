### 类注释
Preferences
Editor -> File and Code Templates -> Files -> Class
```
/**
* @program: ${PROJECT_NAME}
*
* @description: ${description}
*
* @author: Mr.An
*
* @create: ${YEAR}-${MONTH}-${DAY} ${HOUR}:${MINUTE}
**/
```
